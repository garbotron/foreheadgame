const numRooms = 4
const playersPerRow = 3

async function openDb() {
  const app = new Realm.App({ id: 'foreheadgame-nabqo' })
  const user = await app.logIn(Realm.Credentials.anonymous())
  const mongodb = user.mongoClient('mongodb-atlas')
  return mongodb.db('foreheadgame').collection('foreheadgame')
}

async function dbAddPlayer(player) {
  const db = await openDb()
  return await db.insertOne(player)
}

async function dbRemovePlayer(name) {
  const db = await openDb()
  return await db.deleteMany({ name: name })
}

async function dbClearHeads(room) {
  const db = await openDb()
  return await db.updateMany({ room: room }, { $set: { done: false, assignment: "", forehead: "" } })
}

async function dbRemoveRoom(idx) {
  const db = await openDb()
  return await db.deleteMany({ room: idx })
}

async function dbUpsertPlayer(player) {
  const db = await openDb()
  return await db.updateOne({ name: player.name }, { $set: player }, { upsert: true })
}

async function dbGetAllPlayers() {
  const db = await openDb()
  return await db.find()
}

function getUrlParameter(sParam) {
  const sPageURL = decodeURIComponent(window.location.search.substring(1))
  const sURLVariables = sPageURL.split('&')
  let sParameterName
  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=')
    if (sParameterName[0] === sParam) {
      return sParameterName[1] ?  sParameterName[1] : true
    }
  }
}

const currentPlayer = getUrlParameter('player')
const currentRoomTxt = getUrlParameter('room')
const currentRoom = currentRoomTxt ? +currentRoomTxt : undefined

// https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function shuffle(array) {
  let currentIndex = array.length, temporaryValue, randomIndex
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }
  return array
}

function getCookie(name) {
  const nameEQ = name + '='
  const ca = document.cookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1, c.length)
    }
    if (c.indexOf(nameEQ) == 0) {
      return atob(c.substring(nameEQ.length, c.length))
    }
  }
  return undefined
}

function setCookie(name, value, days) {
  let expires = ''
  if (days) {
    const date = new Date()
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
    expires = '; expires=' + date.toUTCString()
  }
  document.cookie = name + '=' + (value ? btoa(value) : '')  + expires + '; path=/'
}

function getNotesCookie() {
  return getCookie('notes')
}

function setNotesCookie(text) {
  setCookie('notes', text, 5)
}

function showLoginScreen() {
  $('#logout').addClass('is-hidden')
  $('#subtitle').text('Tell us your name')
  $('#start-playing').click(() => {
    const name = $('#player-name').val().trim()
    if (name) {
      const uri = location.href.replace(location.search, '')
      window.location.href = `${uri}?player=${encodeURIComponent(name)}`
    } else {
      alert('Please enter a name!')
    }
  })
  $('#player-name').keypress((e) => {
    if (e.keyCode == 13) {
      $('#start-playing').click()
    }
  })
  $('#login-section').removeClass('is-hidden')
}

function getRoomDiv(roomIdx, data) {
  const players = []
  let lastTime

  for (player of data) {
    if (player.room === roomIdx) {
      const playerLastTime = player.lastTime ? new Date(player.lastTime) : undefined
      if (!lastTime || playerLastTime > lastTime) {
        lastTime = playerLastTime
      }
      if (players.length < 2) {
        players.push(player.name)
      } else if (players.length === 2) {
        players.push('...')
      } else {
        // skip the rest
      }
    }
  }

  const playersTxt = players.join(', ')
  const lastTimeTxt = lastTime ? lastTime.toLocaleDateString("en-US") : ''
  const roomUri = `index.html?player=${currentPlayer}&room=${roomIdx}`

  return $('<div>', { 'class': 'tile is-parent' }).append(
    $('<a>', { 'class': 'tile is-child is-large box button', 'href': roomUri }).append(
      $('<p>', { 'class': 'title', text: `Room ${roomIdx}` }),
      $('<p>', { text: playersTxt }),
      $('<p>', { text: lastTimeTxt }),
    )
  )
}

function updateRooms(data) {
  const roomElems = []
  for (let room = 1; room <= numRooms; room++) {
    roomElems.push(getRoomDiv(room, data))
  }
  $('#rooms').append(roomElems)
}

function getFinishRankText(data, player) {
  const prevFinishers = data.filter(x => x.room === player.room && x.done && x.lastTime < player.lastTime)
  const finishRank = 1 + prevFinishers.length
  switch (finishRank) {
    case 1:
      return '1st'
    case 2:
      return '2nd'
    case 3:
      return '3rd'
    default:
      return `${finishRank}th`
  }
}

function getPlayerCard(data, player) {
  const me = player.name.toLowerCase().trim() === currentPlayer.toLowerCase().trim()
  const content = $('<div>', { 'class': 'card-content' }).css('background-color', '#FFFFA5')
  const editorContent = $('<div>', { 'class': 'card-content is-hidden' }).css('background-color', '#FFFFA5')
  const card = $('<div>', { 'class': 'card' }).append(content, editorContent)

  const editInput = $('<input>', { 'class': 'input', 'type': 'text', 'placeholder': 'Forehead text' })
  const editCommit = $('<a>', { 'class': 'button is-primary', text: 'Set' })
  const editCancel = $('<a>', { 'class': 'button', text: 'Cancel' })
  const editField = $('<div>', { 'class': 'field has-addons is-marginless' }).append(
    $('<div>', { 'class': 'control is-expanded' }).append(editInput),
    $('<div>', { 'class': 'control' }).append(editCommit),
    $('<div>', { 'class': 'control' }).append(editCancel)
  )
  editorContent.append($('<p>', { 'class': 'title' }).append(editField))

  editInput.keypress((e) => {
    if (e.keyCode == 13) {
      editCommit.click()
    }
  })

  editCommit.click(() => {
    player.forehead = editInput.val()
    player.givenBy = currentPlayer
    player.lastTime = new Date()
    player.done = false
    dbUpsertPlayer(player)
      .then(() => location.reload(false))
  })

  editCancel.click(() => {
    editorContent.addClass('is-hidden')
    content.removeClass('is-hidden')
  })

  const edit = () => {
    content.addClass('is-hidden')
    editorContent.removeClass('is-hidden')
    editInput.val(player.forehead).select().focus()
  }

  // Add a title line (differs depending on the game state).
  if (me && player.forehead && !player.done) {
    content.append($('<p>', { 'class': 'title', text: `${player.name}: ` }).append($('<em>', { text: 'hidden' })))
  } else if (me && !player.forehead) {
    content.append($('<p>', { 'class': 'title', text: `${player.name}: ` }).append($('<em>', { text: 'blank' })))
  } else if (player.forehead) {
    content.append($('<p>', { 'class': 'title', text: `${player.name}: ${player.forehead}` }))
  } else {
    content.append($('<p>', { 'class': 'title', text: `${player.name}: ` }).append(
      $('<a>').append($('<span>', { text: 'click to set' })).click(edit)
    ))
  }

  // Optionally add a subtitle line, again depending on game state.
  if (me && !player.forehead) {
    content.append($('<p>', { 'class': 'subtitle', text: 'Nothing on your head yet' }))
  } else if (player.forehead) {
    if (player.done) {
      content.append($('<p>', { 'class': 'subtitle', text: `Finished ${getFinishRankText(data, player)}` }))
    } else {
      content.append($('<p>', { 'class': 'subtitle', text: `Written by ${player.givenBy}` }))
    }
  }

  const footerParts = []

  if (me) {
    footerParts.push(
      $('<a>', { 'class': 'card-footer-item' }).append(
        $('<span>', { 'class': 'icon' }).append($('<i>', { 'class': 'fas fa-sticky-note' })),
        $('<span>', { 'class': 'icon-label', text: 'Notes' })
      ).click(() => {
        $('#notes-modal').addClass('is-active')
        $('#notes-modal-textarea').val(getNotesCookie()).focus()
      })
    )
  }

  if (player.forehead && !me) {
    footerParts.push(
      $('<a>', { 'class': 'card-footer-item' }).click(edit).append(
        $('<span>', { 'class': 'icon' }).append($('<i>', { 'class': 'fas fa-edit' })),
        $('<span>', { text: 'Edit' })
      )
    )
  }

  if (player.forehead && (!me || player.done)) {
    const query = encodeURIComponent(player.forehead)
    const googleSearch = `https://www.google.com/search?q=${query}`
    const wikiSearch = `https://en.wikipedia.org/w/index.php?search=${query}`
    footerParts.push(
      $('<a>', { 'class': 'card-footer-item', 'target': '_blank', 'href': googleSearch }).append(
        $('<span>', { 'class': 'icon' }).append($('<i>', { 'class': 'fab fa-google' })),
        $('<span>', { 'class': 'icon-label', text: 'Google' })
      ),
      $('<a>', { 'class': 'card-footer-item', 'target': '_blank', 'href': wikiSearch }).append(
        $('<span>', { 'class': 'icon' }).append($('<i>', { 'class': 'fab fa-wikipedia-w' })),
        $('<span>', { 'class': 'icon-label', text: 'Wiki' })
      )
    )
  }

  if (!me) {
    footerParts.push(
      $('<a>', { 'class': 'card-footer-item' }).append(
        $('<span>', { 'class': 'icon' }).append($('<i>', { 'class': 'fas fa-ban' })),
        $('<span>', { 'class': 'icon-label', text: 'Kick' })
      ).click(() => {
        if (!confirm(`Are you sure you want to kick ${player.name} out of the game?`)) {
          return
        }
        dbRemovePlayer(player.name)
          .then(() => location.reload(false))
      })
    )
  }

  if (footerParts.length === 0) {
    footerParts.push($('<span>', { 'class': 'card-footer-item', html: '&nbsp;' }))
  }

  if (footerParts.length > 2) {
    for (x of footerParts) {
      x.find('.icon-label').addClass('hide-when-tiny')
    }
  }

  card.append($('<footer>', { 'class': 'card-footer' }).append(footerParts))

  if (player.assignment) {
    card.append(
      $('<footer>', { 'class': 'card-footer' }).append(
        $('<span>', { 'class': 'card-footer-item', text: `Assignment: Giving to ${player.assignment}` })
      )
    )
  }

  return card
}

function updatePlayers(data) {
  const playerDivs = []
  let curPlayerDiv
  for (player of data) {
    if (!curPlayerDiv) {
      curPlayerDiv = $('<div>', { 'class': 'columns is-variable is-1' })
      playerDivs.push(curPlayerDiv)
    }
    curPlayerDiv.append($('<div>', { 'class': 'column' }).append(getPlayerCard(data, player)))
    if (curPlayerDiv.children().length == playersPerRow) {
      curPlayerDiv = undefined
    }
  }
  while (curPlayerDiv && curPlayerDiv.children().length < playersPerRow) {
    curPlayerDiv.append($('<div>', { 'class': 'column is-hidden-mobile' }))
  }
  $('#players-container').append(playerDivs)
}

function calcValidAssignmentMap(playerCount) {
  // This is stupid, but it's the easiest way to do it.
  while (true) {
    const assignments = shuffle([...Array(playerCount).keys()])
    if (!assignments.some((x, i) => x === i)) {
      return assignments
    }
  }
}

function updateRoomActions(data) {
  let player = data.find(x => x.name.toLowerCase().trim() === currentPlayer.toLowerCase().trim())
  if (player) {
    if (player.forehead && !player.done) {
      $('#got-it').click(() => {
        player.done = true
        player.lastTime = new Date()
        dbUpsertPlayer(player)
          .then(() => location.reload(false))
      })
      $('#got-it').removeClass('is-hidden')
    }

    $('#leave-game').click(() => {
      if (!confirm('Are you sure you want to leave the game?')) {
        return
      }
      dbRemovePlayer(currentPlayer)
        .then(() => location.reload(false))
    })
    $('#leave-game').removeClass('is-hidden')

  } else {
    $('#join-game').click(() => {
      dbUpsertPlayer({ name: currentPlayer, room: currentRoom, lastTime: new Date() })
        .then(() => location.reload(false))
    })
    $('#join-game').removeClass('is-hidden')
  }

  $('#roll-assignments').click(() => {
    const players = data.filter(x => x.room === currentRoom)
    const assignmentMap = calcValidAssignmentMap(players.length)
    let promise = Promise.resolve()
    for (let i = 0; i < players.length; i++) {
      promise = promise.then(() => dbUpsertPlayer({ ...players[i], assignment: players[assignmentMap[i]].name }))
    }
    promise = promise.then(() => location.reload(false))
  })
  $('#roll-assignments').removeClass('is-hidden')

  $('#clear-all').click(() => {
    if (!confirm('Are you sure you want to clear everyone\'s head?')) {
      return
    }
    dbClearHeads(currentRoom)
      .then(() => location.reload(false))
  })
  $('#clear-all').removeClass('is-hidden')

  $('#reset-game').click(() => {
    if (!confirm('Are you sure you want to reset the game (this will kick all players)?')) {
      return
    }
    dbRemoveRoom(currentRoom)
      .then(() => location.reload(false))
  })
  $('#reset-game').removeClass('is-hidden')

  $('#back-to-room-select').click(() => location.href = `index.html?player=${currentPlayer}`)
  $('#back-to-room-select').removeClass('is-hidden')
}

function showRoomSelectionScreen() {
  $('#subtitle').text(`${currentPlayer}, choose a room`)
  dbGetAllPlayers()
    .then(updateRooms)
    .then(() => $('#rooms-section').removeClass('is-hidden'))
}

function showRoomScreen() {
  $('#subtitle').text(`${currentPlayer}, room ${currentRoom}`)

  $('#notes-modal-title').text(`Notes for ${currentPlayer}`)
  $('#notes-modal-close').click(closeNotesModal);
  $('#notes-modal-clear').click(() => {
    if (!confirm('Are you sure you want to clear your notes?')) {
      return
    }
    $('#notes-modal-textarea').val('').focus()
  })
  $('#notes-modal > .modal-background').click(closeNotesModal)
  $('#notes-modal-textarea').keyup(e => {
    if (e.keyCode == 27) { //escape
      closeNotesModal()
    }
  })

  dbGetAllPlayers()
    .then((data) => {
      updatePlayers(data.filter(x => x.room === currentRoom))
      updateRoomActions(data)
    })
    .then(() => $('#players-section').removeClass('is-hidden'))
    .then(() => $('#room-actions-section').removeClass('is-hidden'))
}

function closeNotesModal() {
  setNotesCookie($('#notes-modal-textarea').val())
  $('#notes-modal').removeClass('is-active')
}

function updateRoot() {
  $('#refresh').click(() => location.reload(false))
  $('#logout').click(() => {
    if (confirm('Are you sure you want to log out?')) {
      window.location.href = 'index.html'
    }
  })
  if (!currentPlayer) {
    showLoginScreen()
  } else if (!currentRoom) {
    showRoomSelectionScreen()
  } else {
    showRoomScreen()
  }
}
